//     tailie.js 0.0.1
//     http://tailie.io
//     (c) 2017 Matthijs van der Kroon, Tailie-JS
//     Tailie-JS may be freely distributed under the MIT license.

(function( TAILIE, undefined ) {
    var self = this;
    self._product_id = null;
    self._win = null; // this will wrap the pop-up login hosted on tailie.io
    self._oauth_ = {}; // _oauth_ object for use with Oauth2 calls, populated by redirect callback

    // Basic setup
    var tailie_base_url="https://apitest.tailie.io";
    var tailie_login_suffix="/api/v2/oauth/authorize";
    var redirect_uri="https://apitest.tailie.io/api/v2/redirect_client";
    // var tailie_base_url="http://localhost:8080";
    // var tailie_login_suffix="/api/v2/oauth/authorize";
    // var redirect_uri="http://localhost:8080/api/v2/redirect_client";
    TAILIE.VERSION = '0.0.4';

    
    TAILIE.init = function(client_id, callback) {
        self.client_id = client_id;
        self.callback = callback

        var all_elements = document.getElementsByClassName("tailie");
        for (i = 0; i < all_elements.length; i++) { 
            var element = all_elements[i];
            
            element.onclick = function(){
                var product_id = this.dataset["product_id"];
                
                if (Object.keys(self._oauth_).length === 0 && self._oauth_.constructor === Object) {
                    // we haven't logged in yet, so open the Oauth2 popup
                    var uri = tailie_base_url + tailie_login_suffix + '?scope=email&client_id=' + client_id + '&response_type=token&redirect_uri=' + redirect_uri;
                    console.log(uri)
                    self._win = window.open(uri, 'Login to Tailie', 'height=' + 450 + ', width=' + 500 + ' ');
                    self.product_id = product_id;
                } else {
                    TAILIE.match(self.callback, function(err) {}, product_id);
                }
            };
        }

        window.addEventListener('message', function(evt) {

            // validate event originates from tailie.io
            if (event.origin.indexOf(tailie_base_url) != -1) {
                
                // close pop-up (if exists)
                if ( self._win !== null) {
                    self._win.close();
                }
                if (Object.keys(self._oauth_).length === 0 && self._oauth_.constructor === Object && self.product_id !== null) {
                    self._oauth_ = evt.data;
                    TAILIE.match(self.callback, function(err) {}, self.product_id);
                    self.product_id = null;
                }
                
                self._oauth_ = evt.data;
            }
        }, false);
    };


    TAILIE.match = function(success, error, product_id) {
        // Oauth2Get(tailie_base_url + '/api/v2/user/' + evt.data['user_id'] + '/match_oauth2?productId=453', evt.data["access_token"], function(d){console.log(d);}, function(d){})
        
        var url = tailie_base_url + '/api/v2/user/' + self._oauth_['user_id'] + "/match_oauth2?productId=" + product_id + "&retailerId=" + self.client_id;

        var xmlhttp = new XMLHttpRequest();   // new HttpRequest instance 
        xmlhttp.onreadystatechange = function() {
            if (this.readyState == 4 && this.status == 200) {
                success(JSON.parse(this.responseText));
            } else {
                error(this)
            }
        };

        xmlhttp.open("GET", url);
        xmlhttp.setRequestHeader('Authorization', 'Bearer ' + self._oauth_["access_token"]); 
        xmlhttp.setRequestHeader("Content-Type", "application/json")
        xmlhttp.send();
    }

}( window.TAILIE = window.TAILIE || {} ));
